﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace Logica
{
    public class Principal
    {
        private static Principal singleton = new Principal();
        private Principal()
        {
        }

        public static Principal Singleton { get { return singleton; } }

        public List<Usuario> ListaUsuarios = new List<Usuario>();
        public List<Movimiento> ListaMovimientos = new List<Movimiento>();


        public Respuesta EnviaryRecibirDinero(Usuario usuario1 , Usuario usuario2, string Descripcion, float Monto)
        {
            Movimiento movimiento = new Movimiento();

            if (ListaUsuarios.Find(x => x.DNI == usuario1.DNI) != null)
            {

                if (ListaUsuarios.Find(x => x.DNI == usuario2.DNI) != null)
                {
                    if (Monto <= usuario1.Saldo)
                    {
                        movimiento.Fecha = DateTime.Today;
                        movimiento.Descripcion = Descripcion;
                        movimiento.Monto = Monto;
                        movimiento.ID += 1;

                        usuario1.Saldo -= movimiento.Monto;
                        usuario2.Saldo += movimiento.Monto;

                        ListaUsuarios.Add(usuario1);
                        ListaUsuarios.Add(usuario2);
                        ListaMovimientos.Add(movimiento);

                        Respuesta.RespuestaInstance.Resultado = true;
                        Respuesta.RespuestaInstance.Detalle = $"El envio de dinero fue exitoso, el emisor es: {usuario1.DNI} y el receptor es: {usuario2.DNI}  ";
                        return Respuesta.RespuestaInstance;
                    }
                    else
                    {
                        Respuesta.RespuestaInstance.Resultado = false;
                        Respuesta.RespuestaInstance.Detalle = $"Fondos insuficientes del usuario Emisor";
                        return Respuesta.RespuestaInstance;
                    }
                }                
                else
                {
                    Respuesta.RespuestaInstance.Resultado = false;
                    Respuesta.RespuestaInstance.Detalle = $"No se encuentra el usuario Receptor";
                    return Respuesta.RespuestaInstance;
                }
                
            }
            else
            {
                Respuesta.RespuestaInstance.Resultado = false;
                Respuesta.RespuestaInstance.Detalle = $"No se encuentra el usuario Emisor";
                return Respuesta.RespuestaInstance;
            }

        }

        public Usuario ObtenerUsuarioPorDNI(int dni)
         {
             if (ListaUsuarios.Find(x => x.DNI == dni) != null)
             {

                 return (ListaUsuarios.Find(x => x.DNI == dni));
             }

             return null;
         }
        public Movimiento ObtenerMovimimientoPorID(int id)
        {
            if (ListaMovimientos.Find(x => x.ID == id) != null)
            {

                return (ListaMovimientos.Find(x => x.ID == id));
            }

            return null;
        }
        public Respuesta EliminarMovimiento(int id)
        {

            if ((ListaMovimientos.Find(x => x.ID == id) != null))
            {
                foreach (var movimiento in ListaMovimientos)
                {
                    ListaMovimientos.Remove(ObtenerMovimimientoPorID(id));
                    Respuesta.RespuestaInstance.Resultado = true;
                    Respuesta.RespuestaInstance.Detalle = $"Cancelacion {movimiento.Descripcion} ";
                    return Respuesta.RespuestaInstance;
                }
            }                      
                Respuesta.RespuestaInstance.Resultado = false;
                Respuesta.RespuestaInstance.Detalle = "El movimiento no existe";
                return Respuesta.RespuestaInstance;           
        }

        public List<Movimiento> ObtenerListadoMovimientoporDNI(int dni)
        {
            List<Movimiento> ListaFiltrada = ListaMovimientos;
            foreach (var usuario in ListaUsuarios)
            {
                if (usuario.DNI == dni)
                {
                    ListaFiltrada.Add(usuario.HistoricoMovimientos);
                }
            }
            ListaFiltrada = ListaFiltrada.OrderByDescending(x => x.Fecha).ToList();
            return ListaFiltrada;
        }

    }
}

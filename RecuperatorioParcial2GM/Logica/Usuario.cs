﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Usuario
    {
        public Usuario(int dNI)
        {
            DNI = dNI;
        }

        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public float Saldo { get; set; }
        public Movimiento HistoricoMovimientos { get; set; }

    }
}

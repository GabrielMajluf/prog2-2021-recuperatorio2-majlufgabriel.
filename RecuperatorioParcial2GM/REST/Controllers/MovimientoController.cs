﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logica;
using REST.Models;

namespace REST.Controllers

{
    public class MovimientoController : ApiController
    {
        public IHttpActionResult Post([FromBody]UsuarioRequest usuarioE, [FromBody]UsuarioRequest usuarioR, string descripcion, float monto)
        {
            Usuario usuario1 = UsuarioRequest.Conversor(usuarioE);
            Usuario usuario2 = UsuarioRequest.Conversor(usuarioR);
            ResponseGeneral responseGeneral = new ResponseGeneral(Logica.Principal.Singleton.EnviaryRecibirDinero(usuario1, usuario2, descripcion, monto));


            if (responseGeneral.Respuesta)
            {
                UsuarioResponse objResponse = new UsuarioResponse(Principal.Singleton.ObtenerUsuarioPorDNI(usuarioE.DNI));
                return Content(HttpStatusCode.Created, objResponse);
            }
            return Content(HttpStatusCode.BadRequest, responseGeneral.Detalle);

        }

        public IHttpActionResult Delete(int id)
        {
            ResponseGeneral responseGeneral = new ResponseGeneral(Principal.Singleton.EliminarMovimiento(id));
            if (responseGeneral.Respuesta)
            {
                return Content(HttpStatusCode.OK, responseGeneral.Detalle);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, responseGeneral.Detalle);
            }
        }

        public IHttpActionResult Get(int dni)
        {
            List<Movimiento> listaMovimientoFitrada = Principal.Singleton.ObtenerListadoMovimientoporDNI(dni);
            List<MovimientoRequest> listado = MovimientoRequest.Conversor(listaMovimientoFitrada);
            if (listado != null)
            {
                return Content(HttpStatusCode.OK, listado);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, "No se movimientos con el dni solicitado");
            }
        }
    }
}

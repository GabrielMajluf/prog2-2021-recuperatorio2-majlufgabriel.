﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.Models
{
    public class MovimientoRequest
    {

        public int ID { get; set; }
        public string Descripcion { get; set; }
        public float Monto { get; set; }

        public MovimientoRequest(Movimiento movimiento)
        {
            this.ID = movimiento.ID;
            this.Descripcion = movimiento.Descripcion;
            this.Monto = movimiento.Monto;
        }

        public static List<MovimientoRequest> Conversor(List<Movimiento> listaMovimientoFitrada)
        {
            List<MovimientoRequest> ReporteMovimientos = new List<MovimientoRequest>();
            foreach (var movimiento in listaMovimientoFitrada)
            {
                MovimientoRequest movimientos = new MovimientoRequest(movimiento);
                ReporteMovimientos.Add(movimientos);
            }
            return ReporteMovimientos;
        }
    }
}
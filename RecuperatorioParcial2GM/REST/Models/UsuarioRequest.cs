﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.Models
{
    public class UsuarioRequest
    {
        public int DNI { get; set; }

        internal static Usuario Conversor(UsuarioRequest usuario)
        {
            Usuario usuario1 = new Usuario(usuario.DNI);
            return usuario1;
        }
    }
}
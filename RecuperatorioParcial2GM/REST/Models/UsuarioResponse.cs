﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.Models
{
    public class UsuarioResponse
    {
        public UsuarioResponse(Usuario usuario)
        {
            this.DNI = usuario.DNI;
        }

        public int DNI { get; set; }
    }
}